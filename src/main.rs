use std::{fs, time::Instant};
use walkdir::WalkDir;
use math_lib::vec2::*;

mod annealing;
use annealing::AnnealingState;

#[repr(u8)]
#[derive(Debug,Copy,Clone,Eq,PartialEq)]
pub enum Key {
    A, B, C, D, E, F, G, H, I, J, K, L, M,
    N, O, P, Q, R, S, T, U, V, W, X, Y, Z,
    Space, Newline, Caps, Nothing,
}
use Key::*;

impl From<usize> for Key {
    fn from(x: usize) -> Self {
        [
            A, B, C, D, E, F, G, H, I, J, K, L, M,
            N, O, P, Q, R, S, T, U, V, W, X, Y, Z,
            Space, Newline, Caps,
        ][x]
    }
}

const PATH: &str = "../projects/";

fn read_training_data() -> Result<Vec<Key>, std::io::Error> {
    let mut r = Vec::new();

    for entry in WalkDir::new(PATH)
        .into_iter()
        .filter_map(|e| e.ok())
        .filter(|e| e.path().extension().map_or(false, |ext| ext == "rs" || ext == "md"))
    {
        let content = fs::read_to_string(entry.path())?;
        for line in content.lines() {
            let mut caps_mode = false;
            let mut iter = line.trim_start().chars().peekable();
            while let Some(c) = iter.next() {
                match c {
                    'a'..='z' => {
                        if caps_mode {
                            r.push(Caps);
                            caps_mode = false;
                        }
                        r.push(Key::from(c as usize - b'a' as usize));
                    }
                    'A'..='Z' => {
                        if !caps_mode {
                            r.push(Caps);
                            if iter.peek().map(|c| c.is_ascii_uppercase()).unwrap_or_default() {
                                r.push(Caps);
                                caps_mode = true;
                            }
                        }
                        r.push(Key::from(c as usize - b'A' as usize));
                    }
                    ' ' => {
                        r.push(Space);
                        caps_mode = false;
                    }
                    '\n' => {
                        r.push(Newline);
                        caps_mode = false;
                    }
                    _ => if r.last() != Some(&Nothing) {
                        r.push(Nothing);
                    }
                }
            }
            r.push(Newline);
        }
    }

    Ok(r)
}

const NUM_KEYS: usize = 29;
const KEY_POSITIONS: [(Vec2<f32>, u8); NUM_KEYS] = [
    //pinkie
    (vec2(0.0, -1.5), 0), (vec2(0.0, -0.5), 0), (vec2(0.0, 0.5), 0), (vec2(0.0, 1.5), 0),
    //ring
    (vec2(0.0, -2.0), 1), (vec2(0.0, -1.0), 1), (vec2(0.0, 0.0), 1), (vec2(0.0, 1.0), 1), (vec2(0.0, 2.0), 1),
    //middle
    (vec2(0.0, -2.0), 2), (vec2(0.0, -1.0), 2), (vec2(0.0, 0.0), 2), (vec2(0.0, 1.0), 2), (vec2(0.0, 2.0), 2),
    //index
    (vec2(-0.5, -2.0), 3), (vec2(-0.5, -1.0), 3), (vec2(-0.5, 0.0), 3), (vec2(-0.5, 1.0), 3), (vec2(-0.5, 2.0), 3),
    (vec2(1.5, -1.0), 3), (vec2(1.5, 1.5), 3), (vec2(1.5, 1.0), 3),
    //thumb
    (vec2(-1.0, -1.0), 4), (vec2(-1.0, 0.0), 4), (vec2(-1.0, 1.0), 4),
    (vec2(0.0, -1.0), 4), (vec2(0.0, 0.0), 4), (vec2(0.0, 1.0), 4),
    (vec2(1.0, 0.0), 4),
];

fn key_cost(pos: Vec2<f32>, finger: u8, prev: Vec2<f32>, prev_time: f32) -> f32 {
    let base = [1.0,0.6,0.0,-0.1,-0.2][finger as usize];
    let dist = 0.5 * (pos.magnitude() + 1.0);
    let travel = distance(pos, prev);
    base + dist / (prev_time + 1.0).ln() + travel / prev_time
}


fn layout_cost(layout: &Layout, data: &[Key]) -> f32 {
    let mut cost = 0.0;
    let mut prev_pos = [Vec2::zero(); 5];
    let mut prev_time = [0.0; 5];

    for &key in data {
        prev_time = prev_time.map(|x| x + 1.0);
        if key == Nothing { continue; }
        let i = layout[key as usize];
        let (pos, finger) = KEY_POSITIONS[i as usize];
        cost += key_cost(pos, finger, prev_pos[finger as usize], prev_time[finger as usize]);
        prev_pos[finger as usize] = pos;
        prev_time[finger as usize] = 0.0;
    }

    cost
}

use plotters::prelude::*;

fn plot_costs(
    overall_costs: &[f32],
    best_costs: &[f32],
    file_path: &str,
) -> Result<(), Box<dyn std::error::Error>> {
    assert_eq!(overall_costs.len(), best_costs.len(), "Cost arrays must have the same length");

    let root_area = BitMapBackend::new(file_path, (640, 480)).into_drawing_area();
    root_area.fill(&WHITE)?;

    let max_cost = *overall_costs.iter().max_by_key(|&&x| x as usize).unwrap();
    let min_cost = *best_costs.last().unwrap();

    let mut chart = ChartBuilder::on(&root_area)
        .caption("Cost Over Time", ("sans-serif", 40).into_font())
        .margin(10)
        .x_label_area_size(20)
        .y_label_area_size(50)
        .build_cartesian_2d(0..overall_costs.len(), min_cost..max_cost)?;

    chart.configure_mesh().draw()?;

    chart.draw_series(LineSeries::new(
        overall_costs.iter().enumerate().map(|(i, &cost)| (i, cost)),
        &RED,
    ))?.label("Overall Cost")
      .legend(|(x, y)| PathElement::new(vec![(x, y), (x + 20, y)], &RED));

    chart.draw_series(LineSeries::new(
        best_costs.iter().enumerate().map(|(i, &cost)| (i, cost)),
        &BLUE,
    ))?.label("Best Cost")
      .legend(|(x, y)| PathElement::new(vec![(x, y), (x + 20, y)], &BLUE));

    chart.configure_series_labels().border_style(&BLACK).draw()?;

    Ok(())
}

type Layout = [u8; NUM_KEYS];
const FIXED_KEYS: [Option<u8>; NUM_KEYS] = {
    let mut r = [None; NUM_KEYS];
    r[Caps as usize] = Some(1);
    r[U as usize] = Some(5);
    r[I as usize] = Some(6);
    r[Y as usize] = Some(9);
    r[A as usize] = Some(10);
    r[E as usize] = Some(11);
    r[O as usize] = Some(12);
    r[D as usize] = Some(15);
    r[T as usize] = Some(16);
    r[S as usize] = Some(17);
    r[Space as usize] = Some(26);
    r[Newline as usize] = Some(27);
    r
};

fn main() -> Result<(), Box<dyn std::error::Error>> {
    let mut layout = [0; NUM_KEYS];
    for i in 0..NUM_KEYS {
        if let Some(j) = FIXED_KEYS[i] {
            layout[i] = j;
        }
    }
    let mut x = 0;
    for i in 0..NUM_KEYS {
        if FIXED_KEYS[i].is_none() {
            while layout.contains(&x) { x += 1; }
            layout[i] = x;
        }
    }
    for i in 0..NUM_KEYS {
        if layout[i] >= NUM_KEYS as u8 {
            layout[i] = 0;
            break;
        }
    }
    let mut data = read_training_data()?;
    data.truncate(data.len() / 4);
    let iterations = 20000;
    let temp = 1e4;
    let decay = 1.0 - 2e-4;
    
    let mut state = AnnealingState::new(data, layout, temp, decay);

    let mut t = Instant::now();
    let start = Instant::now();
    for i in 1..=iterations {
        if i % 500 == 0 {
            println!("500 iterations took {}s",t.elapsed().as_secs());
            println!("{}",state.best_cost);
            println!("{}",state.temp);
            t = Instant::now();
        }
        state.step();
    }
    for _ in 0..(iterations/5) {
        state.cost_data.remove(0);
        state.best_cost_data.remove(0);
    }
    println!("total time for {iterations} iterations: {}s",start.elapsed().as_secs());

    println!("Best layout found with cost: {}", layout_cost(&state.best_layout, &state.data));
    let mut inv_layout = [A; NUM_KEYS];
    for i in 0..inv_layout.len() {
        inv_layout[state.best_layout[i] as usize] = Key::from(i);
    }
    println!("\n{:?}",inv_layout);
    plot_costs(&state.cost_data, &state.best_cost_data, "cost.png")?;
    Ok(())
}
