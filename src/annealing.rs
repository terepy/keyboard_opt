use rand::rngs::ThreadRng;
use rand::{Rng,thread_rng};
use std::collections::HashMap;

use crate::*;

pub struct AnnealingState {
    pub current_layout: Layout,
    pub best_layout: Layout,
    pub current_cost: f32,
    pub best_cost: f32,
    pub cost_data: Vec<f32>,
    pub best_cost_data: Vec<f32>,
    pub temp: f32,
    pub decay: f32,
    pub data: Vec<Key>,
    pub rng: ThreadRng,
    pub layout_cost_memo: HashMap<Layout, f32>,
}

impl AnnealingState {
    pub fn new(data: Vec<Key>, layout: Layout, temp: f32, decay: f32) -> Self {
        let cost = layout_cost(&layout, &data);
        let mut layout_cost_memo = HashMap::new();
        layout_cost_memo.insert(layout, cost);
        Self {
            current_layout: layout,
            best_layout: layout,
            current_cost: cost,
            best_cost: cost,
            cost_data: vec![cost],
            best_cost_data: vec![cost],
            temp,
            decay,
            data,
            rng: thread_rng(),
            layout_cost_memo,
        }
    }

    pub fn step(&mut self) -> bool {
        let mut i = self.rng.gen_range(0..NUM_KEYS);
        while FIXED_KEYS[i].is_some() { i = self.rng.gen_range(0..NUM_KEYS); }
        let mut j = i;
        while i == j || FIXED_KEYS[j].is_some() { j = self.rng.gen_range(0..NUM_KEYS); }
        let mut layout = self.current_layout;
        layout.swap(i, j);

        let cost = *self.layout_cost_memo.entry(layout).or_insert_with(|| layout_cost(&layout, &self.data));
        
        if self.accept_move(cost) {
            self.current_layout = layout;
            self.current_cost = cost;
            if cost < self.best_cost {
                self.best_layout = layout;
                self.best_cost = cost;
            }
        }

        self.cost_data.push(self.current_cost);
        self.best_cost_data.push(self.best_cost);

        self.temp *= self.decay;

        true
    }
    
    fn accept_move(&mut self, cost: f32) -> bool {
        if cost < self.current_cost {
            true
        } else {
            let probability = (-((cost - self.current_cost) / self.temp)).exp();
            self.rng.gen::<f32>() < probability
        }
    }
}

